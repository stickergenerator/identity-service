package pl.baluch.stickergenerator.auth;

import io.grpc.ManagedChannel;
import io.micronaut.context.annotation.Factory;
import io.micronaut.grpc.annotation.GrpcChannel;
import jakarta.inject.Singleton;
import pl.baluch.stickergenerator.api.ReactorAccountPrivateServiceGrpc;

@Factory
public class GrpcClients {

    @Singleton
    ReactorAccountPrivateServiceGrpc.ReactorAccountPrivateServiceStub accountPrivateServiceClient(
            @GrpcChannel("${clients.uri.account}") ManagedChannel channel
    ) {
        return ReactorAccountPrivateServiceGrpc.newReactorStub(channel);
    }
}
