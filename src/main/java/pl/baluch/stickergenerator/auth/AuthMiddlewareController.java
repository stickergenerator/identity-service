package pl.baluch.stickergenerator.auth;

import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.security.token.jwt.validator.JwtTokenValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.baluch.stickergenerator.api.Account;
import pl.baluch.stickergenerator.api.GetAccountsPrivateRequest;
import pl.baluch.stickergenerator.api.ReactorAccountPrivateServiceGrpc;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
@Controller("/auth")
@Secured(SecurityRule.IS_ANONYMOUS)
public class AuthMiddlewareController {

    private final JwtTokenValidator validator;
    private final ReactorAccountPrivateServiceGrpc.ReactorAccountPrivateServiceStub accountServiceClient;

    @Get
    public Mono<HttpResponse<String>> auth(HttpHeaders headers) {
        log.info("Auth start...");
        var token = headers.get("Sg-Token");
        var method = headers.get("X-Forwarded-Uri");
        log.info("Authenticating user for method: {}", method);
        if (Objects.isNull(token) || token.isBlank()) {
            log.info("No token provided, skipping authentication");
            return Mono.just(HttpResponse.ok("OK"));
        }
        log.info("Token provided, validating");
        return Mono.from(validator.validateToken(token, null))
                .map(Principal::getName)
                .flatMap(this::authenticate)
                .defaultIfEmpty(HttpResponse.ok("OK"))
                .doOnError(t -> log.error("Error when authenticating user: {}", t.getMessage()))
                .doOnNext(response -> log.info("User {} authenticated successfully for method {}, accounts: {}", response.header("SG-UserID"), method, response.header("SG-Accounts")));
    }

    private Mono<HttpResponse<String>> authenticate(String userId) {
        log.info("Authenticating user {}", userId);
        var accountsRequest = GetAccountsPrivateRequest.newBuilder().setUserId(userId).build();
        return accountServiceClient.getAccounts(accountsRequest)
                .map(response -> response.getAccountsList().stream().map(Account::getId).collect(Collectors.joining(",")))
                .map(accountIds -> HttpResponse.ok("OK").header("SG-UserID", userId).header("SG-Accounts", accountIds));
    }

}

