package pl.baluch.stickergenerator.auth;

import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.token.event.RefreshTokenGeneratedEvent;
import io.micronaut.security.token.refresh.RefreshTokenPersistence;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.reactivestreams.Publisher;
import pl.baluch.stickergenerator.auth.dto.RefreshToken;
import pl.baluch.stickergenerator.auth.dto.User;
import pl.baluch.stickergenerator.auth.repository.RefreshTokenService;
import pl.baluch.stickergenerator.auth.repository.UserService;

@Singleton
@RequiredArgsConstructor
public class JwtRefreshTokenPersistence implements RefreshTokenPersistence {
    private final UserService userService;
    private final RefreshTokenService refreshTokenService;

    @Override
    public void persistToken(RefreshTokenGeneratedEvent event) {
        var userId = new ObjectId(event.getAuthentication().getName());
        userService.findOne(userId)
                .flatMap(user -> refreshTokenService.save(new RefreshToken(event.getRefreshToken(), user)))
                .block();
    }

    @Override
    public Publisher<Authentication> getAuthentication(String refreshToken) {
        return refreshTokenService.findByToken(refreshToken)
                .map(token -> token.getUser().getId().toString())
                .map(Authentication::build);
    }
}
