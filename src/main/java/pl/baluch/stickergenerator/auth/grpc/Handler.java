package pl.baluch.stickergenerator.auth.grpc;

import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.baluch.stickergenerator.api.*;
import pl.baluch.stickergenerator.auth.Authenticator;
import pl.baluch.stickergenerator.auth.service.AuthService;
import reactor.core.publisher.Mono;

@Log4j2
@Singleton
@RequiredArgsConstructor
public class Handler extends ReactorAuthServiceGrpc.AuthServiceImplBase {
    private final AuthService authService;

    @Override
    public Mono<AuthResponse> login(Mono<LoginRequest> request) {
        return request.flatMap(authService::login)
                .doOnNext(value -> log.info("User logged in successfully"))
                .doOnError(t -> log.error("Error when logging in: " + t.getMessage()));
    }

    @Override
    public Mono<AuthResponse> register(Mono<RegisterRequest> request) {
        return request.flatMap(authService::register)
                .doOnNext(value -> log.info("User registered successfully"))
                .doOnError(t -> log.error("Error when registering user: " + t.getMessage()));
    }

    @Override
    public Mono<AuthResponse> refresh(Mono<RefreshRequest> request) {
        return request.map(RefreshRequest::getToken)
                .flatMap(authService::refresh)
                .doOnNext(value -> log.info("User refreshed token successfully"))
                .doOnError(t -> log.error("Error when refreshing token: " + t.getMessage()));
    }

    @Override
    public Mono<MeResponse> me(Mono<MeRequest> request) {
        return request.flatMap(r -> Authenticator.getUserId())
                .flatMap(authService::getUser)
                .map(user -> MeResponse.newBuilder().setUsername(user.getUsername()).build())
                .doOnNext(value -> log.info("User fetched successfully"))
                .doOnError(t -> log.error("Error when fetching user: " + t.getMessage()));
    }
}
