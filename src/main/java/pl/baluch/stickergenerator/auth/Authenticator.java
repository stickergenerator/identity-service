package pl.baluch.stickergenerator.auth;

import io.grpc.*;
import jakarta.inject.Singleton;
import org.bson.types.ObjectId;
import reactor.core.publisher.Mono;

@Singleton
public class Authenticator implements ServerInterceptor {

    public static final Metadata.Key<String> METADATA_USERID_KEY = Metadata.Key.of("SG-UserID", Metadata.ASCII_STRING_MARSHALLER);
    public static final Context.Key<String> CONTEXT_USERID_KEY = Context.key("SG-UserID");

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        return Contexts.interceptCall(Context.current().withValue(CONTEXT_USERID_KEY, headers.get(METADATA_USERID_KEY)), call, headers, next);
    }

    public static Mono<ObjectId> getUserId() {
        return Mono.justOrEmpty(CONTEXT_USERID_KEY.get())
                .map(ObjectId::new)
                .switchIfEmpty(Mono.error(new StatusRuntimeException(Status.UNAUTHENTICATED)));
    }
}
