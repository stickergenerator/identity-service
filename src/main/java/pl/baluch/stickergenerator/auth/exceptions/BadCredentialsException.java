package pl.baluch.stickergenerator.auth.exceptions;

import io.grpc.Status;
import io.grpc.StatusException;

public class BadCredentialsException extends StatusException {
    public BadCredentialsException(Status status, String message) {
        super(status.withDescription(message));
    }
}
