package pl.baluch.stickergenerator.auth.exceptions;

import io.grpc.Status;
import io.grpc.StatusException;

public class AuthException extends StatusException {
    public AuthException(Status status, String message) {
        super(status.withDescription(message));
    }
}
