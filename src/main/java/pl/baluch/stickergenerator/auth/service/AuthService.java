package pl.baluch.stickergenerator.auth.service;

import io.grpc.Status;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.token.jwt.generator.AccessRefreshTokenGenerator;
import io.micronaut.security.token.validator.RefreshTokenValidator;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.crypto.bcrypt.BCrypt;
import pl.baluch.stickergenerator.api.AuthResponse;
import pl.baluch.stickergenerator.api.LoginRequest;
import pl.baluch.stickergenerator.api.RegisterRequest;
import pl.baluch.stickergenerator.api.Tokens;
import pl.baluch.stickergenerator.auth.dto.RefreshToken;
import pl.baluch.stickergenerator.auth.dto.User;
import pl.baluch.stickergenerator.auth.exceptions.AuthException;
import pl.baluch.stickergenerator.auth.exceptions.BadCredentialsException;
import pl.baluch.stickergenerator.auth.repository.RefreshTokenService;
import pl.baluch.stickergenerator.auth.repository.UserService;
import reactor.core.publisher.Mono;

@Singleton
@RequiredArgsConstructor
public class AuthService {
    private final UserService userService;
    private final RefreshTokenValidator refreshTokenValidator;
    private final AccessRefreshTokenGenerator accessRefreshTokenGenerator;
    private final RefreshTokenService refreshTokenService;


    public Mono<AuthResponse> login(LoginRequest request) {
        return userService.findOneByUsername(request.getUsername())
                .flatMap(user -> {
                    if (!BCrypt.checkpw(request.getPassword(), user.getPassword())) {
                        return Mono.error(new BadCredentialsException(Status.INVALID_ARGUMENT, "Invalid credentials"));
                    }
                    return Mono.just(user);
                })
                .flatMap(this::authenticateUser)
                .switchIfEmpty(Mono.error(new BadCredentialsException(Status.INVALID_ARGUMENT, "Invalid credentials")));
    }

    public Mono<AuthResponse> register(RegisterRequest request) {
        return userService.userExists(request.getUsername())
                .flatMap(exists -> {
                    if (exists) {
                        return Mono.error(new BadCredentialsException(Status.ALREADY_EXISTS, "User already exists"));
                    }
                    var passwordHash = BCrypt.hashpw(request.getPassword(), BCrypt.gensalt());
                    return userService.save(new User(request.getUsername(), passwordHash));
                })
                .flatMap(this::authenticateUser);
    }

    public Mono<AuthResponse> refresh(String token) {
        return Mono.justOrEmpty(refreshTokenValidator.validate(token))
                .flatMap(s -> refreshTokenService.revoke(token))
                .map(RefreshToken::getUser)
                .flatMap(this::authenticateUser)
                .switchIfEmpty(Mono.error(new AuthException(Status.INVALID_ARGUMENT, "Invalid refresh token")));
    }

    private Mono<AuthResponse> authenticateUser(User user) {
        var authentication = Authentication.build(user.getId().toString());
        var tokensResult = accessRefreshTokenGenerator.generate(authentication);
        if (tokensResult.isEmpty()) {
            return Mono.error(new AuthException(Status.INTERNAL, "Failed to generate tokens"));
        }

        var tokens = tokensResult.map(t -> Tokens.newBuilder()
                .setAccessToken(t.getAccessToken())
                .setRefreshToken(t.getRefreshToken())
                .build()).get();

        return Mono.just(AuthResponse.newBuilder()
                .setTokens(tokens)
                .build());
    }

    public Mono<User> getUser(ObjectId id) {
        return userService.findOne(id);
    }
}
