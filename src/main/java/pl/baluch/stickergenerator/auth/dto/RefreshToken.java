package pl.baluch.stickergenerator.auth.dto;

import io.micronaut.core.annotation.Creator;
import io.micronaut.data.annotation.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;

import java.time.Instant;

@Data
@MappedEntity
@RequiredArgsConstructor
public class RefreshToken {
    @Id
    @GeneratedValue
    private ObjectId id;

    private final String token;

    @Relation(Relation.Kind.MANY_TO_ONE)
    private final User user;

    private Boolean revoked = false;

    @DateCreated
    private Instant dateCreated;

    @Creator
    public RefreshToken(ObjectId id, String token, User user, Boolean revoked, Instant dateCreated) {
        this.id = id;
        this.token = token;
        this.user = user;
        this.revoked = revoked;
        this.dateCreated = dateCreated;
    }
}
