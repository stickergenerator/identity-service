package pl.baluch.stickergenerator.auth.dto;

import io.micronaut.core.annotation.Creator;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotBlank;

@Data
@MappedEntity
@RequiredArgsConstructor
public class User {
    @Id
    @GeneratedValue
    private ObjectId id;

    @NonNull
    @NotBlank
    private final String username;

    @NonNull
    @NotBlank
    private final String password;

    @Creator
    public User(ObjectId id, @NonNull String username, @NonNull String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}
