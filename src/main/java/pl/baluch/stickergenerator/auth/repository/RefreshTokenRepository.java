package pl.baluch.stickergenerator.auth.repository;

import io.micronaut.data.annotation.Join;
import io.micronaut.data.mongodb.annotation.MongoRepository;
import io.micronaut.data.repository.reactive.ReactiveStreamsCrudRepository;
import org.bson.types.ObjectId;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import pl.baluch.stickergenerator.auth.dto.RefreshToken;
import reactor.core.publisher.Mono;

@MongoRepository
interface RefreshTokenRepository extends ReactiveStreamsCrudRepository<RefreshToken, ObjectId> {

    @Override
    @NotNull
    @Join("user")
    Publisher<RefreshToken> findById(@NotNull ObjectId objectId);

    @Override
    @NotNull
    @Join("user")
    Publisher<RefreshToken> findAll();

    @NotNull
    @Join("user")
    Mono<RefreshToken> findOneByToken(@NotNull String token);
}
