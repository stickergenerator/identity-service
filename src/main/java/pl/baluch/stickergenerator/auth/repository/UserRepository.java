package pl.baluch.stickergenerator.auth.repository;

import io.micronaut.data.mongodb.annotation.MongoRepository;
import io.micronaut.data.repository.reactive.ReactiveStreamsCrudRepository;
import org.bson.types.ObjectId;
import pl.baluch.stickergenerator.auth.dto.User;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;

@MongoRepository
interface UserRepository extends ReactiveStreamsCrudRepository<User, ObjectId> {

    Mono<User> findOneByUsername(@NotNull String username);

    Mono<Boolean> existsByUsername(@NotNull String username);

}
