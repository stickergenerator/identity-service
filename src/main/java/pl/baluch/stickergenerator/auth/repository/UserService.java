package pl.baluch.stickergenerator.auth.repository;

import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.reactivestreams.client.MongoClient;
import io.micronaut.configuration.mongo.core.DefaultMongoConfiguration;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import pl.baluch.stickergenerator.auth.dto.User;
import reactor.core.publisher.Mono;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class UserService {
    private final MongoClient mongoClient;
    private final DefaultMongoConfiguration configuration;
    private final UserRepository userRepository;

    public Mono<User> findOne(ObjectId id) {
        return Mono.from(userRepository.findById(id));
    }

    public Mono<User> findOneByUsername(String username) {
        return userRepository.findOneByUsername(username);
    }

    public Mono<Boolean> userExists(String username) {
        return userRepository.existsByUsername(username);
    }

    public Mono<User> save(User user) {
        return Mono.from(userRepository.save(user));
    }

    @PostConstruct
    public void init() {
        configuration.getConnectionString().ifPresent(connectionString -> {
            var database = mongoClient.getDatabase(connectionString.getDatabase());
            var collection = database.getCollection("user", User.class);
            var index = Indexes.text("username");
            var options = new IndexOptions().unique(true).name("username_unique");
            Mono.from(collection.createIndex(index, options)).subscribe(
                    s -> log.info("Created index: {}", s),
                    e -> log.error("Error creating index", e)
            );
        });
    }
}
