package pl.baluch.stickergenerator.auth.repository;

import io.grpc.Status;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import pl.baluch.stickergenerator.auth.dto.RefreshToken;
import pl.baluch.stickergenerator.auth.exceptions.AuthException;
import reactor.core.publisher.Mono;

@Singleton
@RequiredArgsConstructor
public class RefreshTokenService {
    private final RefreshTokenRepository repository;

    public Mono<RefreshToken> save(RefreshToken refreshToken) {
        return Mono.from(repository.save(refreshToken));
    }

    public Mono<RefreshToken> findByToken(String token) {
        return repository.findOneByToken(token);
    }

    public Mono<RefreshToken> revoke(String token) {
        return repository.findOneByToken(token)
                .flatMap(refreshToken -> {
                    if(refreshToken.getRevoked()){
                        return Mono.error(new AuthException(Status.INVALID_ARGUMENT, "Refresh token already revoked"));
                    }
                    refreshToken.setRevoked(true);
                    return Mono.from(repository.update(refreshToken));
                });
    }
}
